package com.tsystems.javaschool.tasks.calculator;

import javax.naming.OperationNotSupportedException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Objects;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!validate(statement)) {
            return null;
        }
        String rpn = null;
        try {
            rpn = fromInfixToPostfix(statement);
        } catch (Exception e) {
            return null;
        }
        String processed = process(rpn);
        return format(processed);
    }

    /**
     * Implementation of Shunting-yard algorithm (https://en.wikipedia.org/wiki/Shunting-yard_algorithm)
     *
     * @param statement mathematical expression specified in Infix Notation
     * @return string mathematical expression specified in Reversed Poland Notation
     */
    private String fromInfixToPostfix(String statement) throws Exception {
        Stack<Character> stack = new Stack<>();
        StringBuilder input = new StringBuilder(statement);
        StringBuilder rpn = new StringBuilder(); // Reversed Polish Notation

        while (input.length() > 0) {
            if (input.charAt(0) == ' ') {
                input.deleteCharAt(0);
                continue;
            }
            if (isDigit(input.charAt(0))) {
                String num = readNumber(input);
                rpn.append(num);
                rpn.append(' ');
            } else if (input.charAt(0) == '(') {
                stack.push(input.charAt(0));
                input.deleteCharAt(0);
            } else if (input.charAt(0) == ')') {
                while (!stack.empty() && stack.peek() != '(') {
                    rpn.append(stack.pop());
                    rpn.append(' ');
                }
                if (!stack.empty() && stack.peek() == '(') {
                    stack.pop();
                    input.deleteCharAt(0);
                    continue;
                }
                if (stack.empty()) {
                    throw new Exception();
                }
//                if ()
//                stack.pop();
//                input.deleteCharAt(0);
            } else if (isOperator(input.charAt(0))) {
                while (!stack.empty() && isOperator(stack.peek()) && (priority(stack.peek()) >= priority(input.charAt(0)))) {
                    rpn.append(stack.pop());
                    rpn.append(' ');
                }
                stack.push(input.charAt(0));
                input.deleteCharAt(0);
            } else {
                stack.push(input.charAt(0));
                input.deleteCharAt(0);
            }
        }

        while (!stack.empty()) {
            if (stack.peek() == '(') {
                throw new Exception();
            }
            rpn.append(stack.pop());
            rpn.append(' ');
        }

        return rpn.toString();
    }

    /**
     * Method processing Reverser Poland Notation
     *
     * @param rpn Reversed Poland Notation
     * @return string represented result of processing operations
     */
    private String process(String rpn) {
        String[] ops = rpn.split(" ");
        Stack<String> stack = new Stack<>();
        for (String s :
                ops) {
            if (isOperator(s.charAt(0))) {
                String op1 = stack.pop();
                String op2 = stack.pop();
                try {
                    stack.push(doOperation(s.charAt(0), op2, op1));
                } catch (OperationNotSupportedException e) {
                    return null;
                }
            } else {
                stack.push(s);
            }
        }

        return stack.pop();
    }

    /**
     * Represent result of processing in needed format
     *
     * @param input Number in string
     * @return string formatted number
     */
    private String format(String input) {
        if (Objects.isNull(input)) {
            return null;
        }
        DecimalFormatSymbols decimalFormatSymbols = DecimalFormatSymbols.getInstance();
        decimalFormatSymbols.setDecimalSeparator('.');
        String pattern = "0.####";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        return decimalFormat.format(Double.parseDouble(input));
    }

    /**
     * Read a number from input statement
     *
     * @param builder Input staetment
     * @return string containing a number from statement
     */
    private String readNumber(StringBuilder builder) {
        StringBuilder res = new StringBuilder();
        while (builder.length() > 0 && (builder.charAt(0) == '.' || isDigit(builder.charAt(0)))) {
            res.append(builder.charAt(0));
            builder.deleteCharAt(0);
        }
        return res.toString();
    }

    /**
     * Check character is digit.
     *
     * @param character
     * @return boolean is character digit
     */
    private boolean isDigit(char character) {
        return String.valueOf(character).matches("[0-9]");
    }

    /**
     * Check character is operator
     *
     * @param character
     * @return
     */
    private boolean isOperator(char character) {
        return String.valueOf(character).matches("[+-/*]");
    }

    /**
     * For Shunting-yard algorithm operands must priority.
     *
     * @param op operand
     * @return priority of operator
     */
    private int priority(char op) {
        if (op == '+' || op == '-') {
            return 0;
        }

        if (op == '*' || op == '/') {
            return 1;
        }

        throw new RuntimeException();
    }

    /**
     * Do operation by given operator with operands
     *
     * @param operator char that represent operator of operation
     * @param operand1 the first operand
     * @param operand2 the second operand
     * @return string result of operation
     * @throws OperationNotSupportedException will throw when incorrect operator passed
     */
    private String doOperation(Character operator, String operand1, String operand2) throws OperationNotSupportedException {
        switch (operator) {
            case '+':
                return String.valueOf(Double.parseDouble(operand1) + Double.parseDouble(operand2));
            case '-':
                return String.valueOf(Double.parseDouble(operand1) - Double.parseDouble(operand2));
            case '*':
                return String.valueOf(Double.parseDouble(operand1) * Double.parseDouble(operand2));
            case '/':
                Double fl = Double.parseDouble(operand2);
                if (fl.equals(0.0)) {
                    throw new OperationNotSupportedException();
                }
                return String.valueOf(Double.parseDouble(operand1) / Double.parseDouble(operand2));
        }
        throw new OperationNotSupportedException();
    }

    /**
     * Check that statement contain correct operations
     *
     * @param statement input statement
     * @return boolean
     */
    private boolean validate(String statement) {
        if (statement == null || statement.equals("") || statement.contains("..") || statement.contains("**") ||
                statement.contains("//") || statement.contains("++") || statement.contains("--") || statement.contains(",")) {
            return false;
        }

        return true;
    }
}