package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (Objects.isNull(x) || Objects.isNull(y)) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }
        if (x.size() > y.size())
            return false;

        boolean inProgress = true;
        int i = 0, j = 0;
        while (inProgress) {
            if (j == x.size()) {
                return true;
            }
            if (i == y.size()) {
                return false;
            }
            if (y.get(i).equals(x.get(j))) {
                i++;
                j++;
            } else {
                i++;
            }
        }
        return false;
    }
}