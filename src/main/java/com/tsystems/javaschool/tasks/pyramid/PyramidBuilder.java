package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    public static void main(String[] args) {
        show(new PyramidBuilder().buildPyramid(Collections.nCopies(Integer.MAX_VALUE - 1, 0)));
    }

    private static void show(int[][] buildPyramid) {
        for (int i = 0; i < buildPyramid.length; i++) {
            for (int j = 0; j < buildPyramid[i].length; j++) {
                System.out.print(String.format("%5d", buildPyramid[i][j]));
            }
            System.out.println();
        }
    }

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        try {
            Collections.sort(inputNumbers);
            int levels = decomposition(inputNumbers.size());
            int[][] res = new int[levels][levels * 2 - 1];
            int cntr = 0;
            for (int i = 0; i < levels; i++) {
                for (int j = 0; j < levels * 2 - 1; j++) {
                    int left = levels - i - 1;
                    for (int k = 0; k < i + 1; k++) {
                        res[i][left + k * 2] = inputNumbers.get(cntr++);
                    }
                    break;
                }
            }
            return res;
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        }
    }

    int decomposition(int length) throws Exception {
        int i = 1;
        while (length > 0) {
            length -= i++;
        }
        if (length < 0) {
            throw new Exception();
        }

        return i - 1;
    }
}